#pragma once
#include <iostream>
class SubstitutionText
{
protected:
	std::string _dictionaryFileName;
public:
	SubstitutionText(std::string a, std::string b);
	~SubstitutionText();
	std::string encrypt();
	std::string decrypt();
};

