#pragma once
#include <iostream>
#include <string>
class PlainText
{
protected:	
	bool _isEncrypted;
	std::string _text;
	static int _NumOfTexts;
public:
	PlainText(std::string word);
	~PlainText();
	bool isEnc();
	std::string getText();
	int appearance();


};

