#pragma once
#include <iostream>
#include "PlainText.h"
class ShiftText : public PlainText
{
protected:
	int _key;
public:
	ShiftText(std::string str, int n);
	~ShiftText();
	std::string encrypt();
	std::string decrypt();
};

