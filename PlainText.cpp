#include "PlainText.h"

int PlainText::_NumOfTexts = 0;

PlainText::PlainText(std::string word)
{
	_NumOfTexts += 1;
	this->_text = word;
	this->_isEncrypted = false;
}
PlainText::~PlainText()
{
	this->_isEncrypted = false;
	this->_text.clear();
}
bool PlainText::isEnc()
{
	return this->_isEncrypted;
}
std::string PlainText::getText()
{
	return this->_text;
}

int PlainText::appearance()
{
	return this->_NumOfTexts;
}
