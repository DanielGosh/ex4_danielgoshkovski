#include "ShiftText.h"

ShiftText::ShiftText(std::string str, int n) : PlainText(str)
{
	this->_key = n;
	this->_text = encrypt();
	std::cout << this->_text << "\n";
}

ShiftText::~ShiftText()
{
	this->_key = 0;
	this->_text.clear();
	this->_isEncrypted = false;
}

std::string ShiftText::encrypt()
{
	std::string text = this->_text;
	int key = this->_key;
	char ch;
	for (int i = 0; i < text.length(); i++) {
		ch = text[i];
		if (ch >= 'a' && ch <= 'z') {
			ch = ch + key;
			if (ch > 'z') {
				ch = ch - 'z' + 'a' - 1;
			}
			text[i] = ch;
		}
	}
	this->_isEncrypted = true;
	return text;
}
std::string ShiftText::decrypt()
{
	std::string text = this->_text;
	int key = this->_key;
	char ch;
	for (int i = 0; i < text.length(); ++i) {
		ch = text[i];
		//decrypt for lowercase letter
		if (ch >= 'a' && ch <= 'z') {
			ch = ch - key;
			if (ch < 'a') {
				ch = ch + 'z' - 'a' + 1;
			}
			text[i] = ch;
		}
	}
	this->_isEncrypted = false;
	return text;
}
