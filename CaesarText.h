#pragma once
#include <iostream>
#include "ShiftText.h"
class CaesarText : public ShiftText
{
public:
	CaesarText(std::string str);
	~CaesarText();
	std::string encrypt();
	std::string decrypt();
};

