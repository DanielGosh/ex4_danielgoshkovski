#include "CaesarText.h"

CaesarText::CaesarText(std::string str) : ShiftText(str, 3)
{
	this->_text = this->encrypt();
}

CaesarText::~CaesarText()
{
	this->_key = 0;
	this->_text.clear();
	this->_isEncrypted = false;
}

std::string CaesarText::encrypt()
{
	std::string text = this->_text;
	int key = this->_key;
	char ch;
	for (int i = 0; i < text.length(); i++) {
		ch = text[i];
		if (ch >= 'a' && ch <= 'z') {
			ch = ch + key;
			if (ch > 'z') {
				ch = ch - 'z' + 'a' - 1;
			}
			text[i] = ch;
		}
	}
	this->_isEncrypted = true;
	return text;
}

std::string CaesarText::decrypt()
{
	std::string text = this->_text;
	int key = this->_key;
	char ch;
	for (int i = 0; i < text.length(); ++i) {
		ch = text[i];
		//decrypt for lowercase letter
		if (ch >= 'a' && ch <= 'z') {
			ch = ch - key;
			if (ch < 'a') {
				ch = ch + 'z' - 'a' + 1;
			}
			text[i] = ch;
		}
	}
	this->_isEncrypted = false;
	return text;
}
